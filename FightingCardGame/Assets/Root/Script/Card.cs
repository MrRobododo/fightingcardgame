using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Card",menuName = "Card" )]
public class Card : ScriptableObject
{
    public string name;
    public string desciption;
    public string typetext;

    public Sprite artwork;

    public int range;
    public int damages;
    public int startup;
    public int hitstun;
    public int blockhit;
    
    public void Print ()
    {
        Debug.Log(name + " : ");
    }
}
