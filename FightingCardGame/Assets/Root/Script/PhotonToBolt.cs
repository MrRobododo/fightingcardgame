using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using Bolt;

public class PhotonToBolt : MonoBehaviourPunCallbacks
{
   public  void onConnectedToMaster()
    {
        CustomEvent.Trigger(gameObject, "OnConnectedToMaster");
    }
    public  void onDisconnected(DisconnectCause cause)
    {
        CustomEvent.Trigger(gameObject, "OnDisconnected", cause);
    }
    public  void OnJoinedRoom()
    {
        CustomEvent.Trigger(gameObject, "OnJoinedRoom");
    }
    public  void OnnJoinRandomFailed(short returnCode,string message)
    {
        CustomEvent.Trigger(gameObject, "OnJoinRandomFailed", returnCode, message);
    }
}
