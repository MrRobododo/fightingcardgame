using UnityEngine;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System;

public struct Montage
{
    [XmlElement("questionText")]
    public string questionText;

    [XmlElement("answer")]
    public string answer;
}

[XmlRoot("Questions"), XmlType("Questions")]
public class ConfigScene
{

    [XmlArray("Questions")]
    [XmlArrayItem("Question")]
    public List<Montage> questions = new List<Montage>();

    public static ConfigScene Load(string path)
    {
        try
        {
            XmlSerializer serializer = new XmlSerializer(typeof(ConfigScene));
            using (FileStream stream = new FileStream(path, FileMode.Open))
            {
                return serializer.Deserialize(stream) as ConfigScene;
            }
        }
        catch (Exception e)
        {
            UnityEngine.Debug.LogError("Exception loading config file: " + e);

            return null;
        }
    }
}
/*public TextAsset xmlRawFile;
public Text uiText;
//     Use this for initialization
void Start()
{
    string data = xmlRawFile.text;
    parseXmlFile(data);
}
void parseXmlFile(string xmlData)
{
    string totVal = "";
    XmlDocument xmlDoc = new XmlDocument();
    xmlDoc.Load(new StringReader(xmlData));
    string xmlPathPattern = "//Questions/Question";
    XmlNodeList myNodeList = xmlDoc.SelectNodes(xmlPathPattern);
    foreach (XmlNode node in myNodeList)
    {
        XmlNode questionText = node.FirstChild;
        XmlNode answer = questionText.NextSibling;
        totVal += "questionText: " + Name.InnerXml + "\n answer: " + Tag.InnerXml + "\n\n";
        Debug.Log("List" + totVal);
        uiText.text = totVal;
    }
}*/